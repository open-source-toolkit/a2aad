# YTU 计算机网络课程设计

## 项目描述

本项目是一个用于煤矿生产环境监测的物联网系统，旨在通过实时监测井下环境参数和控制安全设施，确保矿井下的安全生产。项目采用Python语言编写，适用于计算机网络课程设计，也可作为Python课程设计的参考。

## 功能概述

1. **环境监测**：
   - 实时监测井下甲烷、瓦斯、氧气、一氧化碳、温度、烟雾、粉尘等传感器数据。
   - 数据分为模拟量（如甲烷含量、当前温度等）和开关量（如烟雾的有无，用1和0表示）。

2. **设备控制**：
   - 控制通风开关、电扇、报警蜂鸣器等联动设备。
   - 通风设备设有主要和备用两个，主备设备不能同时处于相同状态，以确保设备故障时的应急处理。

3. **系统结构**：
   - 井上服务器运行上位机软件，负责数据接收和处理。
   - 井下嵌入式主机运行下位机软件，负责传感器数据采集和设备控制。

## 项目结构

```
YTU-Computer-Network-Course-Design/
├── README.md
├── src/
│   ├── server/
│   │   ├── main.py
│   │   └── ...
│   └── embedded/
│       ├── main.py
│       └── ...
├── data/
│   ├── sensor_data.csv
│   └── ...
├── docs/
│   ├── design_document.md
│   └── ...
└── requirements.txt
```

## 使用说明

1. **安装依赖**：
   ```bash
   pip install -r requirements.txt
   ```

2. **运行服务器**：
   ```bash
   cd src/server
   python main.py
   ```

3. **运行嵌入式主机**：
   ```bash
   cd src/embedded
   python main.py
   ```

4. **查看数据**：
   - 传感器数据存储在`data/sensor_data.csv`文件中。
   - 可以通过上位机软件实时查看和分析数据。

## 贡献

欢迎对本项目进行改进和扩展。请遵循以下步骤：

1. Fork 本仓库。
2. 创建新的分支 (`git checkout -b feature/your-feature`)。
3. 提交更改 (`git commit -am 'Add some feature'`)。
4. 推送到分支 (`git push origin feature/your-feature`)。
5. 创建新的 Pull Request。

## 许可证

本项目采用 MIT 许可证，详情请参阅 [LICENSE](LICENSE) 文件。

## 联系

如有任何问题或建议，请通过 [GitHub Issues](https://github.com/your-repo/issues) 联系我们。

---

**注意**：本项目仅供学习和参考，实际应用中请根据具体情况进行调整和优化。